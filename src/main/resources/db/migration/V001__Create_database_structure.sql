CREATE TABLE car
(
    id           SERIAL NOT NULL PRIMARY KEY,
    brand        TEXT   NOT NULL,
    model        TEXT   NOT NULL,
    plate_number TEXT   NOT NULL,
    color        TEXT   NOT NULL
);

CREATE TABLE person
(
    id                SERIAL    NOT NULL PRIMARY KEY,
    name              TEXT      NOT NULL,
    surname           TEXT      NOT NULL,
    birth_date        TIMESTAMP NOT NULL,
    age               INT       NOT NULL,
    gender            TEXT      NOT NULL,
    hair_color        TEXT      NOT NULL,
    profession        TEXT      NOT NULL,
    years_of_practice INT       NOT NULL,
    nationality       TEXT      NOT NULL,
    marital_status    TEXT      NOT NULL,
    shoe_size         INT       NOT NULL
);
