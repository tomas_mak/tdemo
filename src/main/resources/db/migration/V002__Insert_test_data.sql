INSERT INTO car(brand, model, plate_number, color)
VALUES ('Hyundai', 'i30', 'KE123OK', 'WHITE'),
       ('Skoda', 'Fabia', 'BB111FR', 'BLUE');

INSERT INTO person (name, surname, birth_date, age, gender, hair_color, profession, years_of_practice, nationality, marital_status, shoe_size)
VALUES ('Janko', 'Mrkvicka', '1992-03-10 10:25:44.000000', 12, 'MALE', 'BROWN', 'programmer', 6, 'SVK', 'MARRIED', 42);
