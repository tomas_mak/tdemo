package com.tsystems.tdemo.mapper;

import com.tsystems.tdemo.dto.CarDto;
import com.tsystems.tdemo.dto.PersonDto;
import com.tsystems.tdemo.entity.Car;
import com.tsystems.tdemo.entity.Person;

public class DtoMapper {

    public CarDto carToCarDto(Car car) {
        CarDto dto = new CarDto();
        dto.setBrand(car.getBrand());
        dto.setModel(car.getModel());
        dto.setPlateNumber(car.getPlateNumber());
        dto.setColor(car.getColor());
        return dto;
    }

    public PersonDto personToPersonDto(Person person) {
        PersonDto dto = new PersonDto();
        dto.setName(person.getName());
        dto.setSurname(person.getSurname());
        dto.setBirthDate(person.getBirthDate());
        dto.setAge(person.getAge());
        dto.setGender(person.getGender());
        dto.setHairColor(person.getHairColor());
        dto.setProfession(person.getProfession());
        dto.setYearsOfPractice(person.getYearsOfPractice());
        dto.setNationality(person.getNationality());
        dto.setMaritalStatus(person.getMaritalStatus());
        dto.setShoeSize(person.getShoeSize());
        return dto;
    }

}
