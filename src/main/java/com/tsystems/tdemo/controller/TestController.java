package com.tsystems.tdemo.controller;

import com.tsystems.tdemo.dto.CarDto;
import com.tsystems.tdemo.dto.PersonDto;
import com.tsystems.tdemo.mapper.DtoMapper;
import com.tsystems.tdemo.repository.CarRepository;
import com.tsystems.tdemo.repository.PersonRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TestController {
    private final CarRepository carRepository;
    private final PersonRepository personRepository;
    private final DtoMapper dtoMapper;

    public TestController(CarRepository carRepository, PersonRepository personRepository) {
        this.carRepository = carRepository;
        this.personRepository = personRepository;
        this.dtoMapper = new DtoMapper();
    }

    @GetMapping(path = "/hello", produces = MediaType.APPLICATION_JSON_VALUE)
    public String hello() {
        return "{\"greetings\": \"Hello world!\"}";
    }

    @GetMapping(path = "/car", produces = MediaType.APPLICATION_JSON_VALUE)
    public CarDto car() {
        String plateNumber = "   ke-123-su       ";

        CarDto car = new CarDto();
        car.setBrand("Mercedes");
        car.setPlateNumber(plateNumber.toUpperCase().trim());

        car.drive(130);

        return car;
    }

    @GetMapping(path = "/cars")
    public List<CarDto> cars() {
        return carRepository
                .findAll()
                .stream()
                .map(dtoMapper::carToCarDto)
                .collect(Collectors.toList());
    }

    @GetMapping(path = "/people")
    public List<PersonDto> people() {
        return personRepository
                .findAll()
                .stream()
                .map(dtoMapper::personToPersonDto)
                .collect(Collectors.toList());
    }
}
