package com.tsystems.tdemo.dto;

public class CarDto {
    private String brand;
    private String model;
    private String plateNumber;
    private String color;

    public CarDto() {
    }

    public CarDto(String brand, String model, String plateNumber, String color) {
        this.brand = brand;
        this.model = model;
        this.plateNumber = plateNumber;
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void drive(int speed) {
        System.out.println("I am driving really fast. My speed is " + speed + "km/h.");
    }
}
